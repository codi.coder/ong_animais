import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from 'react-bootstrap/Card';
import { Container } from "react-bootstrap";
import Row from 'react-bootstrap/Row';

import ListagemList from './components/listagem-list.component'

function App() {
  return (
    <Router>
      <Navbar bg="dark" variant="dark" className="justify-content-center">
        <Nav className="flex-column flex-sm-column">
          <Navbar.Brand style={{ padding: '.5rem' }}>ONG Animalzinhos</Navbar.Brand>
        </Nav>
      </Navbar>
      <Container fluid style={{marginTop:'1%'}}>
                <Row className="justify-content-md-center">
                <Card className="text-center mr-3" style={{ width: '90%', padding:'1%'}}>
                    <Card.Body>
                        <Row className="justify-content-md-center">
                            <Card.Title style={{ padding: '.5rem'}}>Bem-vindo, selecione uma opção.</Card.Title>
                        </Row>
                        <Row className="justify-content-md-center">
                          <Nav>
                            <Nav.Link href="/listagem">Animais Desaparecidos</Nav.Link>
                            {/* <Nav.Link href="/entrar">Login</Nav.Link>   */}
                            {/* <Nav.Link href="/registro">Registrar</Nav.Link>   */}
                            {/* <Nav.Link href="/registro">Registrar</Nav.Link>   */}
                          </Nav>
                        </Row>
                    </Card.Body>
                </Card>
                </Row>
            </Container>
      <Switch>
        <Route exact path="/listagem" component={ListagemList} />
        {/* <Route exact path="/entrar" component={Entrar} /> */}
        {/* <Route exact path="/registro" component={OrdersList} /> */}
        {/* <Route exact path="/orders/add" component={OrdersAdd} /> */}
        {/* <Route exact path="/quotations/:id" component={QuotationsView} /> */}
      </Switch>
    </Router>
  );
}

export default App;
