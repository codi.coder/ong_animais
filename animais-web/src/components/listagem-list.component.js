import React, { Component } from "react";
import ListagemDataService from "../services/listagem.service";


import Table from 'react-bootstrap/Table';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Container } from "react-bootstrap";
import Row from 'react-bootstrap/Row';

export default class ListagemList extends Component {
    constructor(props) {
        super(props);
        this.getListagem = this.getListagem.bind(this);
        this.updateListagem = this.updateListagem.bind(this);

        this.state = {
            listagem: []
        };
    }

    componentDidMount() {
        this.getListagem()
    }

    getListagem() {
        ListagemDataService.getAll()
        .then(response => {
            this.setState({
                listagem: response.data
            });
        })
        .catch(e => {

            console.log(e);
        })
    }

    updateListagem(id){
        var data = {
            sujeito:{
                name:'teste',
                telefone:'123456'
            },
            animal:{
                status:'Comunicado'
            }
        };
        ListagemDataService.update(id,data)
        .then(response =>{
            this.getListagem();
        },
        error =>{
            const resMessage = 
                (error.response &&
                    error.response.data &&
                    error.response.data.message) ||
                error.message ||
                error.toString();

            this.setState({
                message: resMessage,
            });
        })
    }

    render() {
        const { listagem } = this.state;
        console.log(this.state);

        return (

            <Container fluid style={{marginTop:'2%'}}>
                <Row className="justify-content-md-center">
                    <Card className="text-center mr-3" style={{ width: '90%', padding:'1%'}}>
                        <Card.Body>
                            <Row className="justify-content-md-center">
                                <Card.Title style={{ paddingBottom:'2%'}}>Animais</Card.Title>
                            </Row>
                            {/* <Row style={{paddingBottom:'2%'}}>
                                <Button variant="primary" href="/orders/add">Novo Pedido</Button>
                            </Row> */}
                            <Row className="justify-content-md-center">
                                <Table aria-label="customized table">
                                    <thead>
                                        <tr>
                                            <th align="center">ID</th>
                                            <th align="center">Foto</th>
                                            <th align="center">Nome</th>
                                            <th align="center">Idade</th>
                                            <th align="center">Informações</th>
                                            <th align="center">Cidade</th>
                                            <th align="center">Estado</th>
                                            <th align="center">Status</th>
                                            <th align="center">Comunicar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {listagem && listagem.map((list, index) => (
                                            <tr key={index}>
                                                    <th align="center">{list.id}</th>
                                                    <th align="center">{list.foto}</th>
                                                    <th align="center">{list.name}</th>
                                                    <th align="center">{list.idade}</th>
                                                    <th align="center">{list.info_extra}</th>
                                                    <th align="center">{list.cidade}</th>
                                                    <th align="center">{list.estado}</th>
                                                    <th align="center">{list.status}</th>
                                                    <th align="center">
                                                        <Button variant="danger" onClick={() => this.updateListagem(list.id)}>Comunicar</Button>
                                                    </th>
                                            
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </Row>
                    </Card.Body>
                </Card>
                </Row>
            </Container>
        );
    }
    
}