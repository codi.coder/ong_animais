import http from "../http-common";

class ListagemDataService {
    getAll(page) {
        return http.get('/listagem');
    }
    update(id,data) {
        return http.put('/listagem/'+id,data);
    }

}

export default new ListagemDataService();