class Sujeito < ApplicationRecord
  belongs_to :animal
  validates :name, :telefone, presence: true
end