class Animal < ApplicationRecord
  belongs_to :user
  validates :foto, :name , :user, :idade, :status, presence:true
  
  has_many :sujeito, dependent: :destroy
end
