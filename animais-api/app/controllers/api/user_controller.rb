class Api::UserController < ApplicationController
    def new
        @user = User.new
    end
    def create
        @user = User.new(user_params)
        if @user.save
            render json: @user, status: :created
        else
            render json: @user.errors, satus: :unprocessable_entity
        end
    end
    def index
        @user = User.all
        render json: @user
    end
    private
    def user_params
        params.require(:user).permit(:name, :email, :telefone, :password, :password_confirmation)
    end
end
