class Api::AutenticacaoController < ApplicationController

    before_action :set_log_param, only: :create
    before_action :set_user, only: :create

    def create
        if @user.valid_password?(set_log_param[:password])
            render json: @user
        else
            render json: @user.errors, status: :unprocessable_entity
        end
    end

    private
        def set_user
            @user = User.find_by email: set_log_param[:email]
        end
        def set_log_param
            params.require(:log).permit :email, :password
        end

end
