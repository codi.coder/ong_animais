class Api::AnimalController < Api::ApiController
    before_action :set_animal, only: [:show,:update,:destroy]
    before_action :require_autorization!, only:[:show,:update,:destroy]

    #Get /api/animal
    def index
        @animal = current_user.animal
        render json:@animal
    end

    def show
        render json:@animal
    end

    def create
        @animal = Animal.new(animal_params.merge(user: current_user))

        if @animal.save
            render json: @animal, status: :created
        else
            render json: @animal.errors, status: :unprocessable_entity
        end
    end

    def update
        if @animal.update(animal_params)
            render json: @animal
        else
            render json: @animal.errors, status: :unprocessable_entity
        end
    end

    def destroy
        @animal.destroy
    end

    private
        def set_animal
            @animal = Animal.find(params[:id])
        end

        def animal_params
            params.required(:animal).permit(:foto,:name,:idade,:info_extra,:cidade,:estado,:status)
        end

        def require_autorization!
            unless current_user == @animal.user
                render json:{}, status:forbidden
            end
        end
    
end
