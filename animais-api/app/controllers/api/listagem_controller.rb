class Api::ListagemController < ApplicationController
    before_action :set_animal, only: [:update]
    
    def index
        @animal = Animal.all
        paginate  @animal, per_page: 10
        # render json:@animal
    end

    def update
        @sujeito = Sujeito.new(sujeito_params.merge(animal: @animal))
        if @sujeito.save
            if @animal.update(animal_params) 
                render json: @sujeito
            else
                render json: @sujeito.errors, status: :unprocessable_entity
            end
        else
            render json: @sujeito.errors, status: :unprocessable_entity
        end
    end

    private
        def set_animal
            @animal = Animal.find(params[:id])
        end
        def animal_params
            params.required(:animal).permit(:status)
        end
        def sujeito_params
            params.required(:sujeito).permit(:name,:telefone)
        end
end
