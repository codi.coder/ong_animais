class CreateSujeitos < ActiveRecord::Migration[6.0]
  def change
    create_table :sujeitos do |t|
      t.string :name
      t.string :telefone
      t.references :animal, null: false, foreign_key: true

      t.timestamps
    end
  end
end
