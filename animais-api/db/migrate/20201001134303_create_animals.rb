class CreateAnimals < ActiveRecord::Migration[6.0]
  def change
    create_table :animals do |t|
      t.string :name
      t.string :foto
      t.string :idade
      t.text :info_extra
      t.string :cidade
      t.string :estado
      t.string :status
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
