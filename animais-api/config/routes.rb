Rails.application.routes.draw do
  devise_for :users

  namespace:api do
    resources:animal
    resources:listagem
    resources:user
    resources:autenticacao
  end
end
